package com.cgi;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.cgi.model.Deliverer;

public class App {

	public static void main(String[] args) {

		System.out.println("Give me date in ISO format: YYYY-MM-DD");
		Scanner scaner = new Scanner(System.in);
		String date = scaner.nextLine();

		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-M-dd EEE");

		DateTime fdate = DateTime.parse(date, DateTimeFormat.forPattern("yyyy-M-d"));
		System.out.println(dtf.print(fdate));

		DateTime startDate = fdate.dayOfMonth().withMinimumValue().dayOfWeek().withMinimumValue();
		DateTime endDate = fdate.dayOfMonth().withMaximumValue().dayOfWeek().withMaximumValue();
		
		
		Deliverer d = new Deliverer();
		List<DateTime> calendar = new ArrayList<>();
		Days dbetween = Days.daysBetween(startDate, endDate);
		
		
		
		for (int i = 0; i < dbetween.getDays()+1; i++) {
			calendar.add(startDate.plusDays(i));
		}

		d.printCalendar(calendar);
		
		
ScheduledExecutorService schedule = Executors.newScheduledThreadPool(1);
		
		
		final ScheduledFuture<?> timer = schedule.scheduleAtFixedRate(new Runnable() {
			public void run() {
				SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
				Date date = new Date(System.currentTimeMillis());
				System.out.print("\r Current time: " + formater.format(date)) ;
			}
		}, 1, 1, TimeUnit.SECONDS);
		schedule.schedule(new Runnable() {
			public void run() {
				timer.cancel(true);
			}
		}, 10, TimeUnit.MINUTES);

	}
	
	
	

}
